+++
date = "2018-11-15"
weight = 100

title = "webkit2gtk-contextual-zoom"

aliases = [
    "/old-wiki/QA/Test_Cases/webkit2gtk-contextual-zoom"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
