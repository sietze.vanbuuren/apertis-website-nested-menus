+++
date = "2017-05-11"
weight = 100

title = "Presentations"
+++

These are some of the slides and presentations which members of the
Apertis community have previously shared (ordered newest to oldest):

# 2019

- [Building an entire Debian derivative from Git](https://shadura.me/talks/debconf/2019/distro-in-git)
  - The Apertis approach to using git as part of the build pipeline by Andrej
    Shadura

# 2018

- [Testing your distribution with LAVA](https://shadura.me/talks/lvee/2018/testing-your-distribution-with-lava)
  - Introducton to testing with LAVA by Andrej Shadura and Guillaume Tucker

# 2017

- [Managing build infrastructure for a Debian derivative](https://www.youtube.com/watch?v=xuHKOrs_1GY)
  - Introduction on the build and integration infrastructure of Apertis by
    Andrew Shadura

- [Application Framework APIs in action - a case study](/images/genivi-amm-2017-slides.odp)
  - Introduction to application framework for GENIVI and Apertis by Ekaterina
    Gerasimova and Andre Magalhães

# 2016

- [Building an automotive platform from GNOME](https://media.ccc.de/v/53-building_an_automotive_platform_from_gnome)
  - An introduction to Apertis by Lukas Nack

- [Apertis-GNOMEAsia-2016-slides.odp](/images/apertis-gnomeasia-2016-slides.odp)
  - An overview of architecture and development by Sudarshan Chikkapura
    Puttalingaiah
