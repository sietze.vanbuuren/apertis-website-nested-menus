+++
date = "2018-05-14"
weight = 100

title = "traffic-control-tcdemo"

aliases = [
    "/old-wiki/QA/Test_Cases/traffic-control-tcdemo"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
