+++
date = "2022-03-18"
weight = 100

title = "v2023dev1 Release Notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2023dev1** is the second **development** of the Apertis
v2023 stable release flow that will lead to the LTS **Apertis v2023.0**
release in March 2023.

This Apertis release is built on top of Debian Bullseye with several
customisations. It currently
ships with the Linux kernel 5.15.x LTS series but later releases in the
v2023 channel will track newer kernel versions up to the next LTS.

Test results for the v2023dev1 release are available in the following
test reports:

  - [APT images](https://lavaphabbridge.apertis.org/report/v2022/20220309.0316/apt)
  - [OSTree images](https://lavaphabbridge.apertis.org/report/v2022/20220309.0316/ostree)
  - [NFS artifacts](https://lavaphabbridge.apertis.org/report/v2022/20220309.0316/nfs)
  - [LXC containers](https://lavaphabbridge.apertis.org/report/v2022/20220309.0316/lxc)

## Release flow

  - 2021 Q4: v2023dev0
  - **2022 Q1: v2023dev1**
  - 2022 Q2: v2023dev2
  - 2022 Q3: v2023dev3
  - 2022 Q4: v2023pre
  - 2023 Q1: v2023.0
  - 2023 Q2: v2023.1
  - 2023 Q3: v2023.2
  - 2023 Q4: v2023.3
  - 2024 Q1: v2023.4
  - 2024 Q2: v2023.5
  - 2024 Q3: v2023.6
  - 2024 Q4: v2023.7

### Release downloads

| [Apertis v2023dev1.0 images](https://images.apertis.org/release/v2023dev1/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2023dev1/v2023dev1.0/amd64/fixedfunction/apertis_ostree_v2023dev1-fixedfunction-amd64-uefi_v2023dev1.0.img.gz) | [hmi](https://images.apertis.org/release/v2023dev1/v2023dev1.0/amd64/hmi/apertis_ostree_v2023dev1-hmi-amd64-uefi_v2023dev1.0.img.gz) | [base SDK](https://images.apertis.org/release/v2023dev1/v2023dev1.0/amd64/basesdk/apertis_v2023dev1-basesdk-amd64-sdk_v2023dev1.0.ova) | [SDK](https://images.apertis.org/release/v2023dev1/v2023dev1.0/amd64/sdk/apertis_v2023dev1-sdk-amd64-sdk_v2023dev1.0.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2023dev1/v2023dev1.0/armhf/fixedfunction/apertis_ostree_v2023dev1-fixedfunction-armhf-uboot_v2023dev1.0.img.gz) | [hmi](https://images.apertis.org/release/v2023dev1/v2023dev1.0/armhf/hmi/apertis_ostree_v2023dev1-hmi-armhf-uboot_v2023dev1.0.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2023dev1/v2023dev1.0/arm64/fixedfunction/apertis_ostree_v2023dev1-fixedfunction-arm64-uboot_v2023dev1.0.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2023dev1/v2023dev1.0/arm64/fixedfunction/apertis_ostree_v2023dev1-fixedfunction-arm64-rpi64_v2023dev1.0.img.gz) | [hmi](https://images.apertis.org/release/v2023dev1/v2023dev1.0/arm64/hmi/apertis_ostree_v2023dev1-hmi-arm64-rpi64_v2023dev1.0.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (MinnowBoard Turbot Dual-Core)]( {{< ref "/reference_hardware/amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2023dev1 repositories

    deb https://repositories.apertis.org/apertis/ v2023dev1 target development sdk


## New features

### Completion of the GPL-3-free modernization

This release completes the modernization of the approach to
[licensing expectations compliance]({{< ref "license-expectations.md" >}})
started with the
[v2022dev2]({{< ref "release/v2022dev2/releasenotes.md" >}}) release.

The [v2022.0]({{< ref "release/v2022.0/releasenotes.md" >}}) release already
addressed the licensing challenges in the
[TLS stack]({{< ref "tls-stack.md" >}}),
[Coreutils]({{< ref "coreutils-replacement.md" >}}), and
[GnuPG]({{< ref "gnupg-replacement.md" >}}) and this release addresses all the
smaller challenges identified in the
[GPL-3 Deltas Assessment]({{< ref "gpl3_free_deltas.md" >}}),
in particular the move to `rust-findutils` and `libedit`.

## Build and integration

### Build-test packages on OBS before merging

A new feature of the CI for packaging is the inclusion of OBS build test on branches
to be merged. Thanks to this addition, now developers can confirm that their changes
successfully build in OBS before being merged in the main branches.

### Improve developer experience while importing packages

The infrastructure around Apertis has been improved in order to make the importing of new
packages straightforward, by avoiding as much as possible any manual steps. With these
changes the effort invested to import new packages has been reduced drastically and the possibility
of errors in the process has been notably reduced.

### Automated note importing in QA Report App

The QA report app is the tool used to collect and summarize the test results for all the Apertis images.
To improve the user experience for testers running manual tests and to reduce
the amount of information introduced, now the results notes are
[automatically propagated](https://gitlab.apertis.org/infrastructure/qa-report-app/-/merge_requests/121)
from previous submissions, giving the chance to the user to amend them to match
the current results.

## Documentation and designs

### Thin proxies - REST APIs

This release includes a concept document [Thin proxies: REST APIS]({{ < ref "thin-proxies.md" >}})
with guidelines for developers to access
system APIs through high level REST APIs. Using this approach the complexity of
system APIs can be hidden and developers can chose the language/technology that better
fit their needs.

### Documentation refresh to match the switch to Flatpak

Continuing with the effort of updating documentation after switching to Flatpak, this
release also includes a reviewed version of several documents related to the Application
Framework to provide developers with the best practices to build their applications.

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations

### Breaks

#### GPL-3 replacements revamp on target images (HMI and FixedFunction)

The obsolete `findutils-gplv2` and `readline5` packages are not longer
available in the `target` packaging repositories.

The HMI and FixedFunction images now use the `rust-findutils` tools and the
API-compatible `libedit` to be compliant with the
[Apertis licensing expectations]({{< ref "license-expectations.md" >}})
while avoiding unmaintained forks of GPL-3 components.

See the [GPL-3 Deltas Assessment]({{< ref "gpl3_free_deltas.md" >}}) document
for further details.

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-testcases-builder),
Docker images.

### Apertis infrastructure tools

The [Apertis v2022 infrastructure repository](https://build.collabora.co.uk/project/show/apertis:infrastructure:v2022)
provides packages for the required versions of `ostree-push` and
`ostree` for Debian Buster:

    deb https://repositories.apertis.org/infrastructure-v2022/ buster infrastructure

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

