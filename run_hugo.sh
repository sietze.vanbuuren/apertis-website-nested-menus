#!/bin/bash

REPO=$(dirname $(realpath $0))

docker run \
  --rm \
  -p 1313:1313/tcp \
  -w /source \
  -u $(id -u) \
  -v ${REPO}:/source \
  --security-opt label=disable \
  klakegg/hugo:latest \
  $@

