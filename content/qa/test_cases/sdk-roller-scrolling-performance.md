+++
date = "2017-10-27"
weight = 100

title = "sdk-roller-scrolling-performance"

aliases = [
    "/old-wiki/QA/Test_Cases/sdk-roller-scrolling-performance"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
