+++
date = "2020-10-01"
weight = 100

title = "Releases"
+++

This section contains the release notes and schedules for all current and previous releases of Apertis. Please refer to the [release schedule]({{< ref "releases.md" >}}) and [release flow]({{< ref "release-flow.md" >}}) for more information.

