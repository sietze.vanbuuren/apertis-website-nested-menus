+++
date = "2017-10-23"
weight = 100

title = "clutter-performance"

aliases = [
    "/old-wiki/QA/Test_Cases/clutter-performance"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
