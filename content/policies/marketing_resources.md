+++
date = "2017-05-11"
weight = 100

title = "Marketing Resources"

aliases = [
    "/old-wiki/Marketing_resources"
]
+++

# The Apertis Logo

The Apertis logo consists of 2 elements:

1. The Symbol
2. The Wordmark

In most situations the Symbol and the Workmark should remain connected and
used as a whole. In isolated cases, where suitable, it is permitted that
the Symbol can be separated from the Workmark, likewise the Wordmark can
be removed from the Symbol.

When using the Symbol and Wordmark togther, please ensure that you
maintain the spacing /layout between them as below.

The Apertis logo are copyright Collabora Ltd and licensed under
[BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).

The logo can be used to represent Apertis. Use them on slides and when
talking about Apertis.

## Symbol and Wordmark

| PNG (100x100px) | PNG (500x500px) | SVG |
| --------------- | --------------- | --- |
| ![100×100 px](/images/apertis-100.png) | ![500×500 px](/images/apertis-500.png) | ![SVG](/images/apertis_logo.svg) |

## Symbol

| PNG (100x100px)                  | PNG (500x500px)                  |
| -------------------------------- | -------------------------------- |
| ![100×100 px](/images/a-100.png) | ![500×500 px](/images/a-500.png) |

